package tests;

import static org.junit.Assert.*;

import java.io.BufferedWriter;
import java.util.ArrayList;
import java.util.List;

import entities.Product;
import org.junit.Test;

import services.CustomerService;
import services.CustomerServiceInterface;
import entities.Customer;

public class CustomerServiceTest {

	private static final String ERROR_MESSAGE = "result cant't be null";

	@Test
	public void testFindByName() {
		//GIVEN
		CustomerServiceInterface cs = new CustomerService(DataProducer.getTestData(10));
		//WHEN
		List<Customer> res = cs.findByName("Customer: 1");
		//THEN
		assertNotNull(ERROR_MESSAGE, res);
		assertEquals(1, res.size());
		assertEquals("Customer: 1", res.get(0).getName());
	}

	@Test
	public void testCustomersWhoBoughtMoreThan() throws IllegalAccessException {
		//GIVEN
		CustomerServiceInterface cs = new CustomerService(DataProducer.getTestData(10));
		//WHEN
		List<Customer> res = cs.customersWhoBoughtMoreThan(3);
		//THEN
		assertNotNull(ERROR_MESSAGE, res);
		assertEquals(4, res.size());
		assertEquals(7, res.get(0).getBoughtProducts().size());
		assertEquals(6, res.get(1).getBoughtProducts().size());
		assertEquals(5, res.get(2).getBoughtProducts().size());
		assertEquals(4, res.get(3).getBoughtProducts().size());
	}

	@Test
	public void testFindByField_givenName() {
		//GIVEN
		CustomerServiceInterface cs = new CustomerService(DataProducer.getTestData(10));
		String targetCustomerName = "Customer: 1";
		//WHEN
		List<Customer> res = cs.findByField("name", targetCustomerName);
		//THEN
		assertNotNull(ERROR_MESSAGE, res);
		assertEquals(1, res.size());
		assertEquals(targetCustomerName, res.get(0).getName());
	}

	@Test
	public void testFindByField_givenProductList() {
		//GIVEN
		List<Product> targetProducts = new ArrayList<>();
		targetProducts.add(new Product(5, "Product: 5", 0.5));
		targetProducts.add(new Product(4, "Product: 4", 0.4));
		CustomerServiceInterface cs = new CustomerService(DataProducer.getTestData(10));
		//WHEN
		List<Customer> res = cs.findByField("boughtProducts", targetProducts);
		//THEN
		assertNotNull(ERROR_MESSAGE, res);
		assertEquals(1, res.size());
		assertEquals(targetProducts, res.get(0).getBoughtProducts());
	}

	@Test
	public void testFindCustomersWhoSpentMoreThan_givenNoMatchingCustomers() {
		//GIVEN
		CustomerServiceInterface cs = new CustomerService(DataProducer.getTestData(5));
		//WHEN
		List<Customer> res = cs.customersWhoSpentMoreThan(10000);
		//THEN
		assertNotNull(ERROR_MESSAGE, res);
		assertEquals(0, res.size());
	}

	@Test
	public void testFindCustomersWhoSpentMoreThan_givenOneMatchingCustomer() {
		//GIVEN
		CustomerServiceInterface cs = new CustomerService(DataProducer.getTestData(6));
		//WHEN
		List<Customer> res = cs.customersWhoSpentMoreThan(1);
		//THEN
		assertNotNull(ERROR_MESSAGE, res);
		assertEquals(1, res.size());
	}

	@Test
	public void testFindCustomersWithNoOrders() {
		//GIVEN
		CustomerServiceInterface cs = new CustomerService(DataProducer.getTestData(6));
		//WHEN
		List<Customer> res = cs.customersWithNoOrders();
		//THEN
		assertNotNull(ERROR_MESSAGE, res);
		assertEquals(3, res.size());
	}

	@Test
	public void testAddProductToAllCustomers() {
		//GIVEN
		Product product = new Product(1234509876, "mySpecialProduct", 80);
		CustomerServiceInterface cs = new CustomerService(DataProducer.getTestData(3));
		//WHEN
		cs.addProductToAllCustomers(product);
		//THEN
		assertEquals(cs.getCustomers().get(0).getBoughtProducts().get(0), product);
		assertEquals(cs.getCustomers().get(1).getBoughtProducts().get(0), product);
	}

	@Test
	public void testAvgOrders_notIncludeEmpty() {
		//GIVEN
		CustomerServiceInterface cs = new CustomerService(DataProducer.getTestData(4));
		Double expected = 0.4;
		//WHEN
		Double res = cs.avgOrders(false);
		//THEN
		assertEquals(expected, res);
	}

	@Test
	public void testAvgOrders_includeEmpty() {
		//GIVEN
		CustomerServiceInterface cs = new CustomerService(DataProducer.getTestData(4));
		Double expected = (0.0 + 0.0 + 0.0 + 0.4)/4;
		//WHEN
		Double res = cs.avgOrders(true);
		//THEN
		assertEquals(expected, res);
	}

	@Test
	public void testWasProductBought_givenNegativeAnswer() {
		//GIVEN
		CustomerServiceInterface cs = new CustomerService(DataProducer.getTestData(4));
		Product product = new Product(1234509876, "mySpecialProduct", 80);
		//WHEN
		boolean res = cs.wasProductBought(product);
		//THEN
		assertFalse(res);
	}

	@Test
	public void testWasProductBought_givenPositiveAnswer() {
		//GIVEN
		CustomerServiceInterface cs = new CustomerService(DataProducer.getTestData(10));
		Product product = new Product(4, "Product: 4", 0.4);
		//WHEN
		boolean res = cs.wasProductBought(product);
		//THEN
		assertTrue(res);
	}

	@Test
	public void testGetMostPopularProduct_givenSingle() {
		//GIVEN
		CustomerServiceInterface cs = new CustomerService(DataProducer.getTestData(10));
		Product product = new Product(4, "Product: 4", 0.4);
		//WHEN
		List<Product> res = cs.mostPopularProduct();
		//THEN
		assertNotNull(ERROR_MESSAGE, res);
		assertEquals(1, res.size());
		assertEquals(product, res.get(0));
	}

	@Test
	public void testGetMostPopularProduct_givenDouble() {
		//GIVEN
		CustomerServiceInterface cs = new CustomerService(DataProducer.getTestData(3));
		Product product1 = new Product(44324242, "myProduct", 40);
		Product product2 = new Product(1234509876, "mySpecialProduct", 80);
		cs.addProductToAllCustomers(product1);
		cs.addProductToAllCustomers(product2);
		//WHEN
		List<Product> res = cs.mostPopularProduct();
		//THEN
		assertNotNull(ERROR_MESSAGE, res);
		assertEquals(2, res.size());
		assertTrue(res.get(0).equals(product1) || res.get(0).equals(product2));
		assertTrue(res.get(1).equals(product1) || res.get(1).equals(product2));
	}

	@Test
	public void testCountBuys() {
		//GIVEN
		CustomerServiceInterface cs = new CustomerService(DataProducer.getTestData(10));
		Product product = new Product(4, "Product: 4", 0.4);
		Integer expected = 7;
		//WHEN
		Integer res = cs.countBuys(product);
		//THEN
		assertEquals(expected, res);
	}

	@Test
	public void testCountCustomersWhoBought() {
		//GIVEN
		CustomerServiceInterface cs = new CustomerService(DataProducer.getTestData(10));
		Product product = new Product(4, "Product: 4", 0.4);
		Integer expected = 7;
		//WHEN
		Integer res = cs.countCustomersWhoBought(product);
		//THEN
		assertEquals(expected, res);
	}
}

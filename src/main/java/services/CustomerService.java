package services;

import entities.Customer;
import entities.Product;

import java.lang.reflect.Field;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class CustomerService implements CustomerServiceInterface {

    private List<Customer> customers;

    public CustomerService(List<Customer> customers) {
        this.customers = customers;
    }

    public List<Customer> getCustomers() {
        return customers;
    }

    @Override
    public List<Customer> findByName(String name) {
        return customers.stream()
                .filter(customer -> customer.getName().equals(name))
                .collect(Collectors.toList());
    }

    @Override
    public List<Customer> findByField(String fieldName, Object value) {
        return customers.stream().filter(c ->
                {
                    boolean result = false;
                    try {
                        Field field = Customer.class.getDeclaredField(fieldName);
                        field.setAccessible(true);
                        result = field.get(c).equals(value);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return result;
                }
        ).collect(Collectors.toList());
    }


    @Override
    public List<Customer> customersWhoBoughtMoreThan(int number) {
        return customers.stream()
                .filter(customer -> customer.getBoughtProducts().size() > number)
                .collect(Collectors.toList());
    }

    @Override
    public List<Customer> customersWhoSpentMoreThan(double price) {
        return customers.stream().
                filter(customer ->
                {
                    List<Double> prices = customer.getBoughtProducts().stream().
                            map(Product::getPrice).
                            collect(Collectors.toList());
                    return prices.stream().mapToDouble(Double::doubleValue).sum() > price;
                }).
                collect(Collectors.toList());
    }

    @Override
    public List<Customer> customersWithNoOrders() {
        return customers.stream().
                filter(customer ->
                        customer.getBoughtProducts().size() == 0).
                collect(Collectors.toList());
    }

    @Override
    public void addProductToAllCustomers(Product p) {
        customers.forEach(Customer -> Customer.addProduct(p));
    }

    @Override
    public double avgOrders(boolean includeEmpty) {
        List<Customer> properCustomers = customers.stream().
                filter(customer ->
                        (includeEmpty || customer.getBoughtProducts().size() > 0)).
                collect(Collectors.toList());
        return properCustomers.stream().
                map(customer -> {
                            List<Double> prices = customer.getBoughtProducts().stream().
                                    map(Product::getPrice).
                                    collect(Collectors.toList());
                            return prices.stream().mapToDouble(Double::doubleValue).sum();
                        }
                ).mapToDouble(Double::doubleValue).average().orElse(-1);
    }

    @Override
    public boolean wasProductBought(Product p) {
        return customers.stream().anyMatch(customer ->
                customer.getBoughtProducts().stream().anyMatch(product -> product.equals(p)));
    }

    @Override
    public List<Product> mostPopularProduct() {
        Collection<Product> products = customers.stream()
                .flatMap(customer -> customer.getBoughtProducts().stream())
                .collect(Collectors.toList());

        return products.stream().collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
                .entrySet().stream()
                .collect(Collectors.groupingBy(Map.Entry::getValue, Collectors.mapping(Map.Entry::getKey, Collectors.toList())))
                .entrySet().stream().max(Comparator.comparing(Map.Entry::getKey)).map(Map.Entry::getValue)
                .orElse(Collections.emptyList());
    }

    @Override
    public int countBuys(Product p) {
        return (int) customers.stream()
                .flatMap(customer -> customer.getBoughtProducts().stream())
                .filter(product -> product.equals(p)).count();
    }

    @Override
    public int countCustomersWhoBought(Product p) {
        return (int) customers.stream().filter(customer -> customer.getBoughtProducts().contains(p)).count();
    }

}
